$(document).ready(function () {

  function scrollAnimation() {
    $('a[href="#top"]').on('click', function (event) {
      event.preventDefault();
      if (window.innerWidth <= 769 && ($('span.close').length > 0) ) {
        $('.navigation__menu-list').toggle('display');
        $('span.navigation__mobile-menu').toggleClass('close');
        $("html, body").animate({ scrollTop: 0 }, 1000);
        return false;        
      }
      $("html, body").animate({ scrollTop: 0 }, 1000);
      return false;
    });
    $('a[href^="#"]').on('click', function (event) {
      var target = $(this.getAttribute('href'));
      if (target.length) {
        event.preventDefault();
        if (window.innerWidth <= 769) {
          $('.navigation__menu-list').toggle('display');
          $('span.navigation__mobile-menu').toggleClass('close');          
          $('html, body').stop().animate({
            scrollTop: target.offset().top - 75
          }, 1000);
        }
        $('html, body').stop().animate({
          scrollTop: target.offset().top - 75
        }, 1000);
      }
    });
  }

  function paralax() {
    $(window).scroll(function () {
      var x = $(this).scrollTop();
      if (window.innerWidth <= 1000) {
        $('.main-img').css('background-position', '25% ' + parseInt(60 + x / 30) + '%');
        $('.main-img').css('filter', 'blur(' + parseInt(x / 55) + 'px)');
      } else {
        $('.main-img').css('background-position', 'center ' + parseInt(60 + x / 30) + '%');
        $('.main-img').css('filter', 'blur(' + parseInt(x / 55) + 'px)');
      }
      if (x > 560) {
        if (window.innerWidth <= 1000) {
          $('.second-img').css('background-position', '35% ' + parseInt(24 + x / 45) + '%');
        }
        if (window.innerWidth <= 568) {
          $('.second-img').css('background-position', '35% ' + parseInt(10 + x / 25) + '%');
          return;
        }
        if(window.innerWidth > 1000) {
          $('.second-img').css('background-position', 'center ' + parseInt(20 + x / 45) + '%');
        }
      }
    });
  }

  function changeLanguage(event) {
    $('#pl-en').on('click', function (event) {
      $('.pl').toggleClass('hidden');
      $('.eng').toggleClass('hidden');
    });
    $('#en-pl').on('click', function (event) {
      $('.eng').toggleClass('hidden');
      $('.pl').toggleClass('hidden');
    });
  }

  function mobileMenuIconHandler() {
    var icon = $('.navigation__mobile-menu');
    icon.on('click', function (e) {
      e.stopPropagation();
      $(this).toggleClass('close');
      // $('.navigation__wrapper').toggleClass('visable');
      $('.navigation__menu-list').toggle('display');
    });
  }

  changeLanguage();
  scrollAnimation();
  paralax();
  mobileMenuIconHandler();
});
